# BBPress to HumHub Migration

## Note: this is only intended to import users, posts and replies to a clean humhub installation.  Use at your own risk!

Determination of queries was based upon a diff of database dumps before and after a post and reply were created

* I searched and replaced some values in these before pushing for privacy, may have introduced bugs, again use at own risk
* These scripts preserve ids, should only be used on a clean humhub install.
* Does not create spaces or deal with multiple forums, my site just had one. Extend for your own purposes
* table name prefixes may differ
* will need to manually rebuilds search indexes when complete. See https://www.humhub.org/docs/guide-admin-adv-search.html
* activity window will reflect order of load vs order of activity. eg: post post post comment comment comment, not the correct date order.
* You could combine scripts to preserve this but wasn't important to me.
* PHP max_execution_time will likely need to be increased in order to run these scripts

## migrate_users.php
* Loads all wordpress users
* adds to humhub user table, and to default space.
* preserves username, and email only. Users will be registered but will need to password reset to login.
* preserves user ids for ease of migrating posts and replies.  Only use on a clean install!

## migrate_posts
* Loads all forum posts
* adds to appropriate humhub tables
* prepends post title to post on a separate line
* preserves post ids for ease of migrating replies

## migrate_replies
* Loads all forum replies
* adds to appropriate humhub tables
* preserves ids

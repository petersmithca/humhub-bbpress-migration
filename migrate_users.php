<?php
/*
This script migrates bb_press users to humhub
*/
$dest =new PDO("mysql:host=host;dbname=destination", "user", "password");
$dest->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$source =new PDO("mysql:host=host;dbname=source", "user",  "password");
$source->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$sql = "SELECT * from bp_users";
$stmt = $source->prepare($sql);
$stmt->execute();
$results = $stmt->fetchAll();

foreach($results as $user) {
    if ($user['ID'] != 1) {
        $sql = "INSERT INTO user (id, guid, wall_id, group_id, status, super_admin, username, email, auth_mode, language, created_by, visibility, time_zone) " .
            "VALUES (:id, uuid(), :id, 1, 1, 0, :user, :email, 'local', 'en', 1, 1, 'America/Halifax')";
        $stmt = $dest->prepare($sql);
        $stmt->bindValue(':id', $user['ID']);
        $stmt->bindValue(':user', $user['user_login']);
        $stmt->bindValue(':email', $user['user_email']);
        $stmt->execute();

        $sql = "INSERT INTO space_membership(space_id, user_id, status, invite_role, admin_role, share_role) " .
            "VALUES(1, :id, 3, 0, 0, 0)";

        $stmt = $dest->prepare($sql);
        $stmt->bindValue(':id', $user['ID']);

        $stmt->execute();

         $sql = "INSERT INTO profile(user_id, lastname, firstname) " .
            "VALUES(:id, 'LName', 'FName')";

        $stmt = $dest->prepare($sql);
        $stmt->bindValue(':id', $user['ID']);

        $stmt->execute();
    }

}

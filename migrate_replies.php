<?php

/*
This script migrates bb_press replies to humhub
*/
$dest =new PDO("mysql:host=host;dbname=destination", "post", "password");
$dest->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$source =new PDO("mysql:host=host;dbname=source", "post",  "password");
$source->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$sql = "SELECT * FROM bp_posts
WHERE post_type='reply'
ORDER BY post_date ASC";

$stmt = $source->prepare($sql);
$stmt->bindValue(':post', $post);
$stmt->execute();
$results = $stmt->fetchAll();

foreach ($results as $reply) {
    $sql = "INSERT INTO comment(id, message, object_model, object_id, created_at, created_by, updated_at) " .
        "VALUES(:id, :message, :model, :object_id, :date, :user, :date)";

    $stmt = $dest->prepare($sql);
    $stmt->bindValue(':id', $reply['ID']);
    $stmt->bindValue(':model', 'humhub\modules\post\models\Post');
    $stmt->bindValue(':message', $reply['post_content']);
    $stmt->bindValue(':object_id', $reply['post_parent']);
    $stmt->bindValue(':date', $reply['post_date']);
    $stmt->bindValue(':user', $reply['post_author']);

     $stmt->execute();

    $commentId = $dest->lastInsertId();

     $sql = "INSERT into activity (class, module, object_model, object_id) " .
        "VALUES(:path, 'comment', :model, :id )";

    $stmt = $dest->prepare($sql);
    $stmt->bindValue(':id', $commentId);
    $stmt->bindValue(':path', 'humhub\modules\comment\activities\NewComment');
    $stmt->bindValue(':model', 'humhub\modules\comment\models\Comment');
    $stmt->execute();

    $activityId = $dest->lastInsertId();

    $sql = "INSERT into content (guid, object_model, object_id, visibility, sticked, archived, space_id, user_id, created_at, created_by, updated_at) " .
        "VALUES(uuid(), :model, :id, 0, 0, 0, 1, :user, :date, :user, :date )";

    $stmt = $dest->prepare($sql);
    $stmt->bindValue(':id', $activityId);
    $stmt->bindValue(':model', 'humhub\modules\activity\models\Activity');
    $stmt->bindValue(':date', $reply['post_date']);
    $stmt->bindValue(':user', $reply['post_author']);
    $stmt->execute();

    $lastId2 = $dest->lastInsertId();

    $sql = "INSERT into wall_entry (wall_id, content_id, created_at, created_by, updated_at) " .
        "VALUES(2, :content_id , :date, :user, :date )";

    $stmt = $dest->prepare($sql);
    $stmt->bindValue(':content_id', $lastId2);
    $stmt->bindValue(':date', $reply['post_date']);
    $stmt->bindValue(':user', $reply['post_author']);
    $stmt->execute();


}

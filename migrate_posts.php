<?php
/*
This script migrates bb_press posts to humhub - requires preserving post ids
*/
$dest =new PDO("mysql:host=host;dbname=destination", "post", "password");
$dest->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$source =new PDO("mysql:host=host;dbname=source", "post",  "password");
$source->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$sql = "SELECT * FROM bp_posts
WHERE post_type='topic'
ORDER BY post_date ASC";

$stmt = $source->prepare($sql);
$stmt->execute();
$results = $stmt->fetchAll();

foreach ($results as $post) {
    $sql = "INSERT INTO post(id, message, created_at, created_by, updated_at) " .
        "VALUES(:id, :message, :date, :post, :date)";

    $stmt = $dest->prepare($sql);
    $stmt->bindValue(':id', $post['ID']);
    $stmt->bindValue(':message', $post['post_title'] . ':
        ' . $post['post_content']);
    $stmt->bindValue(':date', $post['post_date']);
    $stmt->bindValue(':post', $post['post_author']);

     $stmt->execute();

     $sql = "INSERT INTO post_follow(object_model, object_id, post_id, send_notifications) " .
        "VALUES(:model, :id, :post, 1)";

    $stmt = $dest->prepare($sql);
    $stmt->bindValue(':id', $post['ID']);
    $stmt->bindValue(':model', 'humhub\modules\post\models\Post');
    $stmt->bindValue(':post', $post['post_author']);

     $stmt->execute();

     $sql = "INSERT into activity (class, module, object_model, object_id) " .
        "VALUES(:path, 'content', :model, :id )";

    $stmt = $dest->prepare($sql);
    $stmt->bindValue(':id', $post['ID']);
    $stmt->bindValue(':path', 'humhub\modules\content\activities\ContentCreated');
    $stmt->bindValue(':model', 'humhub\modules\post\models\Post');
    $stmt->execute();

    $activityId = $dest->lastInsertId();

    $sql = "INSERT into content (guid, object_model, object_id, visibility, sticked, archived, space_id, post_id, created_at, created_by, updated_at) " .
        "VALUES(uuid(), :model, :id, 0, 0, 0, 1, :post, :date, :post, :date )";

    $stmt = $dest->prepare($sql);
    $stmt->bindValue(':id', $post['ID']);
    $stmt->bindValue(':model', 'humhub\modules\post\models\Post');
    $stmt->bindValue(':date', $post['post_date']);
    $stmt->bindValue(':post', $post['post_author']);
    $stmt->execute();

    $lastId = $dest->lastInsertId();

    $sql = "INSERT into content (guid, object_model, object_id, visibility, sticked, archived, space_id, post_id, created_at, created_by, updated_at) " .
        "VALUES(uuid(), :model, :id, 0, 0, 0, 1, :post, :date, :post, :date )";

    $stmt = $dest->prepare($sql);
    $stmt->bindValue(':id', $activityId);
    $stmt->bindValue(':model', 'humhub\modules\activity\models\Activity');
    $stmt->bindValue(':date', $post['post_date']);
    $stmt->bindValue(':post', $post['post_author']);
    $stmt->execute();

    $lastId2 = $dest->lastInsertId();

    $sql = "INSERT into wall_entry (wall_id, content_id, created_at, created_by, updated_at) " .
        "VALUES(2, :content_id , :date, :post, :date )";

    $stmt = $dest->prepare($sql);
    $stmt->bindValue(':content_id', $lastId);
    $stmt->bindValue(':date', $post['post_date']);
    $stmt->bindValue(':post', $post['post_author']);
    $stmt->execute();

    $sql = "INSERT into wall_entry (wall_id, content_id, created_at, created_by, updated_at) " .
        "VALUES(2, :content_id , :date, :post, :date )";

    $stmt = $dest->prepare($sql);
    $stmt->bindValue(':content_id', $lastId2);
    $stmt->bindValue(':date', $post['post_date']);
    $stmt->bindValue(':post', $post['post_author']);
    $stmt->execute();


}
